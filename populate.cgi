#!/usr/local/bin/python3

import cgi, json
import sys, traceback
import mysql.connector

sys.stderr = sys.stdout

# Main function
def main():
    print("Content-Type: application/json\n\n")
    
    query = """
    SELECT a.uniquename, a.name
      FROM feature a
           JOIN cvterm ftype ON a.type_id = ftype.cvterm_id
     WHERE ftype.name = 'assembly';
    """
    
    records = { 'iserror' : False, 'ids' : [], 'data' : [] }
    
    conn = mysql.connector.connect(user='rgrady7', password='projecting', host='localhost', database='rgrady7_chado')
    curs = conn.cursor()
    
    curs.execute(query)
    
    for (accn, name) in curs:
        if isinstance(accn, (bytes, bytearray)): accn = accn.decode("utf-8")
        if isinstance(name, (bytes, bytearray)): name = name.decode("utf-8")
        if accn is None: accn = ''
        if name is None: name = ''
        records['ids'].append(accn)
        records['data'].append({'id' : accn, 'name' : name})
    
    print(json.dumps(records))

# Run the main function if we're called.
if __name__ == '__main__':
    main()
