// This function opens the tab for the indicated search mode.
function openTab( caller ) {
	$('.tab-content').each( function (i, item) {
		$(item).hide();
	});
	
	$('.tablink.active').removeClass('active');
	
	$('#' + caller.attr('data-target-id')).show();
	caller.addClass('active');
}


// This function implements the AJAX call to execute the appropriate search.
function runSearch( dataStr ) {
	// Hide & clear any previous results.
	$('#results').hide();
	$('.res-table thead').empty();
	$('.res-table tbody').empty();
	
	// Transform the form parameters into a server-friendly string.
	// var dataStr = caller.serialize();
	
	// Make an AJAX call.
	$.ajax({
		url: './search_form.cgi',
		dataType: 'json',
		data: dataStr,
		success: function(data, textStatus, jqXHR) {
			parseJSON(data);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Failed to perform Chado search! AJAX call returned Text Status '" +
				  textStatus + "' and threw the Error '" + errorThrown + "'.");
		}
	});
}

// This function interprets the JSON returned by the AJAX call.
function parseJSON( d ) {
	if (d.type === 'error') {
		/*
		 * We have an error message, so we raise an alert and exit the function.
		 */
		alert(d.message);
		
		// Now we exit the parsing function.
		return;
	}
	
	// Update the result count.
	if (d.type != "new") {
		$('span#res-count').text( d.count + " result" + (d.count == 1 ? "" : "s" ) + " found.");
	}
	
	
	/*######## ADD COLUMN HEADERS ########*/
	$('<tr/>', { "id" : "res-hdr" }).appendTo('table.res-table thead');
	
	for (i = 0; i < d.cols.length; i++) {
		$('<th/>', {"text" : d.cols[i].name}).appendTo('.res-table #res-hdr');
	}
	
	/*######## ADD RESULT ROWS ########*/
	$.each(d.data, function(i, item) {
		// Temporary variable for current row.
		var row = $('<tr/>');
		
		// Append current row to table body.
		row.appendTo('table.res-table tbody');
		
		// Loop through the list of columns to build the current row.
		$.each(d.cols, function(j, key) {
			var txt;
			
			if (item[key.id] == null) {
				txt = '';
			} else {
				txt = item[key.id];
				// if (typeof txt == "string" && d.type == "add") {
					// txt = txt.replace(/\r?\n/g, '<br/>');
				// }
			}
			
			$('<td/>', {"text" : txt , "class" : key.id }).appendTo(row);
		});
	});
	
	// Show the results
	$('#results').show();
	
	if (d.type === "new" ) {
		// If an item was added, refresh the autocomplete and hide the list of results.
		populate();
		$('#res-count').text('Record successfully added.');
	}
}

/*
 * This function populates the autocomplete and listbox values of the forms,
 * using an AJAX call to get the IDs.
 */
function populate() {
	$.get('./populate.cgi', '', function(data) {
		popData(data)
	}, 'json');
	/* $.ajax({
		url: './populate.cgi',
		dataType: 'json',
		data: "",
		success: function(data, textStatus, jqXHR) {
			popAjax(data);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Failed to populate autocomplete/listbox values! AJAX call returned Text Status '" +
				  textStatus + "' and threw the Error '" + errorThrown + "'.");
		}
	}); */
}

/*
 * Callback function for use in a GET or AJAX call to populate the autocomplete
 * and listbox data.
 */
function popData(d) {
	/* if (d.iserror):
		// If there was an error, print out the data.
		$('<section/>', {'id' : 'errmsg', 'text' : d.trace}).appendTo('body')
		return; */
	var auto = '[name=search_in]';
	// Handle the autocomplete for the Browse option.
	if ($(auto).data('ui-autocomplete') == undefined) {
		// If we don't have autocomplete bound to the text input, we add it.
		$(auto).autocomplete({source : d.ids});
	} else {
		// If we have autocomplete bound to the text input, we simply update the source.
		$(auto).autocomplete('option', 'source', d.ids);
	}
	
	// Update the listbox contents.
	$('#feature-source').empty()
	
	$.each(d.data, function(i, item) {
		var txt = item.id;
		
		if (item.name != '') {
			txt += ' (' + item.name + ')';
		}
		$('<option/>', {'value' : d.data[i].id,
						'text' : txt}).appendTo('#feature-source');
	});
}

// Tie the JavaScript to the form once the page is ready.
$(document).ready( function() {
	// Define the Submit behavior operation for each form.
	$('form').each(function (index) {
		$(this).submit( function() {
			runSearch($(this).serialize());
			return false;
		});
	});
	
	$('.tablink').each(function (i, item) {
		$(this).click( function() {
			openTab($(this))
		});
	});
	
	// Hide the loading icon at the start.
	$('#loading-icon').hide();
	
	/*
	 * Add a loading icon. Code based on snippet here:
	 * https://snippets.aktagon.com/snippets/204-how-to-display-an-animated-icon-during-ajax-request-processing
	 */
	$(document).ajaxSend(function(event, request, settings) {
		$('#loading-icon').show();
	});
	
	$(document).ajaxComplete(function(event, request, settings) {
		$('#loading-icon').hide();
	});
	
	$('.tab-content').hide();
	
	$('#srch-annot').show();
	
	// Populate the listboxes and autocomplete settings.
	populate();
	
	// Hide the results section.
	$('#results').hide();
});