#!/usr/local/bin/python3

import cgi, json
import re
import mysql.connector
from Bio import Entrez, GenBank
from Bio import Seq
from Bio.Alphabet import IUPAC

srchfields = ["f.uniquename", "f.name", "prod.value", "org.abbreviation"]

# If the input is a byte array or a byte sequence, converts it to UTF8
def bconv(s):
    if isinstance(s, (bytes, bytearray)):
        return s.decode("UTF-8")
    else:
        return s

# Simple wrapper function to return a MySQL Connector.
def getconn():
    return mysql.connector.connect(user='rgrady7', password='projecting', host='localhost', database='rgrady7_chado')

# Returns the last insert ID from the given cursor (simple wrapper for query 'SELECT LAST_INSERT_ID();')
# Argument 'c' must be a mysql.connector.cursor object.
def getlastid(c):
    c.execute("SELECT LAST_INSERT_ID();")
    out, = c.fetchone()
    return out

# Function to retrieve annotations matching the given criteria and return the
# results as JSON.
def getannotation(data):
    annot = data.getvalue('search_in')
    regex = data.getvalue('use_regex')
    term = data.getvalue('search_term')
    
    query = """
    SELECT f.uniquename,
           f.name,
           locn.fmin,
           locn.fmax,
           prod.value,
           org.abbreviation
      FROM feature f
           JOIN cvterm ftype ON f.type_id = ftype.cvterm_id
           JOIN featureprop prod ON f.feature_id = prod.feature_id
           JOIN cvterm ptype ON prod.type_id = ptype.cvterm_id
           JOIN featureloc locn ON f.feature_id = locn.feature_id
           JOIN feature src ON locn.srcfeature_id = src.feature_id
           JOIN cvterm srctype ON src.type_id = srctype.cvterm_id
           JOIN organism org ON f.organism_id = org.organism_id
     WHERE (ftype.name IN ('polypeptide', 'mRNA', 'exon', 'gene', 'CDS') AND
            srctype.name = 'assembly' AND
            src.uniquename = %s{:s});
    """
    
    conn = getconn()
    curs = conn.cursor()
    
    if (term != "" and term is not None and 'search_term' in data):
        # Add a conditional for the search term IF we're using one.
        cols = " OR ".join(t + " {0:s} @srch" for t in srchfields)
        query = query.format(" AND ({:s})".format(cols.format("REGEXP" if regex else "LIKE")))
        if regex == 'off':
            # Make sure we use the '%' notation if we're not using Regexes
            term = '%{:s}%'.format(term)
        
        # Set the '@srch' variable
        curs.execute("SET @srch = %s;", (term, ))
    else:
        # We aren't using a search term, so we clear the formatting codes.
        query = query.format("", "", "")
    
    # Execute the search query.
    curs.execute(query, (annot, ))
    
    results = { 'type' : 'annotation', 'count' : 0, 'data' : [] }
    
    # Loop through the rows returned, adding each one to the list of results.
    for (accn, name, start, end, prod, org) in curs:
        results['data'].append({'accn' : bconv(accn), 'name' : bconv(name),
                                'start' : bconv(start), 'end' : bconv(end),
                                'locn' : '{0}..{1}'.format(bconv(start), bconv(end)),
                                'prod' : bconv(prod), 'org' : bconv(org)})
        results['count'] += 1
    
    # Clear the SQL variable
    curs.execute("SET @srch = NULL;")
    
    conn.close()
    
    # List of columns for quick processing of header row and looping through result rows.
    # results['cols'] = [{'name' : 'Annotation ID', 'id' : 'accn'},
                       # {'name' : 'Annotation Name', 'id' : 'name'},
                       # {'name' : '# of Features', 'id' : 'children'},
                       # {'name' : 'Organism', 'id' : 'org'}]
    
    results['cols'] = [{'name' : 'Accession', 'id' : 'accn'},
                       {'name' : 'Gene Name', 'id' : 'name'},
                       {'name' : 'Location', 'id' : 'locn'},
                       {'name' : 'Gene Product', 'id' : 'prod'},
                       {'name' : 'Organism', 'id' : 'org'}]
    
    return json.dumps(results)

def getfeature(data):
    # Boolean indicating whether we should allow Regular Expressions
    regex = data.getvalue('use_regex')
    term = data.getvalue('search_term')
    sources = data.getlist('source')
    
    #   Figure out what conditional statement we need.
    cond = "({:s})".format(" OR ".join(t + " {0:s} @srch" for t in (srchfields + ['src_organism'])))
    cond = cond.format("REGEXP" if regex else "LIKE")
    
    if not regex:
        # Use '%' notation if we're not using Regexes.
        term = '%{:s}%'.format(term)
    
    # If we're only searching within one or more specific annotations, add that constraint
    if len(sources) > 0:
        cond = "src.uniquename IN (%s) AND {:s}".format(cond)
    
    query = """
    SELECT f.uniquename,
           f.name,
           locn.fmin,
           locn.fmax,
           prod.value,
           src.uniquename,
           src.name,
           CONCAT(org.genus, ' ', org.species) AS 'src_organism',
           org.abbreviation
      FROM feature f
           JOIN cvterm ftype ON f.type_id = ftype.cvterm_id
           JOIN featureprop prod ON f.feature_id = prod.feature_id
           JOIN cvterm ptype ON prod.type_id = ptype.cvterm_id
           JOIN featureloc locn ON f.feature_id = locn.feature_id
           JOIN feature src ON locn.srcfeature_id = src.feature_id
           JOIN cvterm srctype ON src.type_id = srctype.cvterm_id
           JOIN organism org ON f.organism_id = org.organism_id
     WHERE (ftype.name IN ('polypeptide', 'mRNA', 'exon', 'gene', 'CDS') AND
            srctype.name = 'assembly' AND
            ptype.name = 'gene_product_name' AND
            {:s});
    """.format(cond)
    
    conn = getconn()
    curs = conn.cursor()
    
    curs.execute("SET @srch = %s", (term, ))
    
    # Build parameters based on the provided search parameters.
    if len(sources) > 0:
        curs.execute(query, (', '.join(sources), ))
    else:
        curs.execute(query)
    
    results = { 'type' : 'feature', 'count' : 0, 'data' : [] }
    
    # Loop through the rows returned, adding each one to the list of results.
    for (accn, name, start, end, prod, srcid, srcname, orgname, orgabbr) in curs:
        results['data'].append({'accn' : bconv(accn), 'name' : bconv(name),
                                'start' : bconv(start), 'end' : bconv(end),
                                'locn' : '{0}..{1}'.format(bconv(start), bconv(end)),
                                'prod' : bconv(prod), 'src_accn' : bconv(srcid),
                                'src_name' : bconv(srcname),
                                'org' : '{0} ({1})'.format(bconv(orgname), bconv(orgabbr))})
        results['count'] += 1
    
    curs.execute("SET @srch = NULL;")
    
    conn.close()
    
    # List of columns for quick processing of header row.
    results['cols'] = [{'name' : 'Accession', 'id' : 'accn'},
                       {'name' : 'Gene Name', 'id' : 'name'},
                       {'name' : 'Location', 'id' : 'locn'},
                       {'name' : 'Gene Product', 'id' : 'prod'},
                       {'name' : 'Annotation Acc.', 'id' : 'src_accn'},
                       {'name' : 'Annotation Name', 'id' : 'src_name'},
                       {'name' : 'Organism', 'id' : 'org'}]
    
    return json.dumps(results)

# Function to add an entry from the GenBank database.
def add(data):
    accn = data.getvalue('new_accn')
    
    conn = getconn()
    curs = conn.cursor(buffered=True)
    
    # First, we check to make sure that the item isn't already in the database.
    curs.execute("""
                 SELECT COUNT(f.uniquename)
                   FROM feature f
                        JOIN cvterm t ON f.type_id = t.cvterm_id
                  WHERE t.name = 'assembly' AND f.uniquename = %s;
                 """, (accn, ))
    
    count = int(curs.fetchone()[0])
    
    if count > 0:
        # We have the item already; return an error.
        return json.dumps({'type' : 'error', 'message' : "Annotation {:s} already exists in the database.".format(accn)})
    
    # Track the number of rows added to each table.
    count = {'feature' : 0,
             'featureloc' : 0,
             'featureprop' : 0,
             'organism' : 0}
    
    # Dictionary mapping qualifier key names (dict keys) to output key names (dict values).
    qualkeys = {'gene' : 'gene_name',
                'product' : 'prod_name',
                'locus_tag' : 'gene_accn'}
    
    Entrez.email = 'rgrady7@jhu.edu'
    
    # Get the results of querying Entrez.
    res = Entrez.read(Entrez.esearch(db='nucleotide', term=accn))
    
    if len(res) < 1:
        return json.dumps({'type' : 'error',
                           'errmsg' : 'No matches found in GenBank for accession "{:s}"'.format(accn)})
    
    # We found at least one match, so we retrieve and parse the appropriate GenBank file.
    gb = GenBank.read(Entrez.efetch(db='nucleotide', id=','.join(res['IdList']), rettype='gb', retmode='text'))
    
    # Cache all feature data in case of unordered feature lists.
    feat = []
    org = {}
    
    for f in gb.features:
        if f.key == 'source' and len(org) == 0:
            # Retrieve the taxid and species+strain name from the GB file.
            for q in f.qualifiers:
                if q.key == '/organism=':
                    org['full_name'] = q.value
                elif q.key == '/db_xref=':
                    org['taxid'] = re.sub(r'^"(?:taxon:)*|"$', '', q.value, re.I)
            
            # The current feature is the 'source' feature AND we haven't defined the source organism yet:
            # Retrieve the necessary taxonomy data from Entrez.
            o = Entrez.read(Entrez.esummary(id=org['taxid'], db='taxonomy', retmode='xml'))[0]
            
            # Retrieve the genus, species, and common name.
            m = re.match(r'(.*?) (.*)$', o['ScientificName'], re.I)
            
            org['genus'] = m.group(1)
            org['species'] = m.group(2)
            
            # Determine which field to use for the common name.
            if not (o['CommonName'] == "" or o['CommonName'] is None):
                # The 'CommonName' field is defined, so we use that.
                org['common'] = o['CommonName']
            else:
                # The 'CommonName' field is undefined, so we use the scientific name.
                org['common'] = o['ScientificName']
            
            # Generate the abbreviation.
            org['abbrev'] = re.sub(r'^(.)(\w*)\b', r'\1.', o['ScientificName'])
            org['abbrev'] = re.sub(r'\b(?:sub)?str\. ', '', org['abbrev'])
        elif f.key == "CDS":      # Only supports CDS records.
            # Handle CDS records.
            ftr = {'ftype' : 'gene', 'gene_name' : '', 'gene_id' : ''}
            
            # Retrieve the start & end points from the 
            m = re.search(r"([<>]?\d+)(?:\.\.([<>]?\d+)(?:(?:,[<>]?\d+\.\.\d+)*,[<>]?\d+\.\.([<>]?\d+))?)?",
                          f.location)
            
            # m = re.search(r"^(?:([<>]?\d+)\.\.([<>]?\d+)|(join|complement)\(([<>]?\d+)\.\.([<>]?\d+)(?:,([<>]?\d+)\.\.([<>]?\d+))?\))$",
                          # f.location, re.I)
            
            ftr['strand'] = 1       # Assume + strand; we correct this later if needed.
            ftr['phase'] = 0        # Assume a phase of 0 (corrected later if needed).
            
            if m is not None:
                ftr['start'] = m.group(1)
                
                if re.search(r"\bjoin\(", f.location, re.I) and m.group(3) is not None:
                    # If this is a join, use the third capture group as the feature end.
                    ftr['end'] = m.group(3)
                elif (m.group(2) is not None):
                    # If this isn't a join AND we have a second capture group,
                    # use the second capture group as the feature end.
                    ftr['end'] = m.group(2)
                else:
                    # This is a single-point feature, use the start as the end.
                    ftr['end'] = ftr['start']
            
            if re.search(r"^(?:join\()?complement", f.location):
                # Update the strand value if this is a complementary sequence.
                ftr['strand'] = -1
            
            # Indicates whether the start and/or end points are partial.
            ftr['partial'] = {'start' : False, 'end' : False}
            
            if re.match(r"[<>]", ftr['start']):
                ftr['partial']['start'] = True
                ftr['start'] = re.sub(r'^[<>]', '', ftr['start'])
            
            if re.match(r"[<>]", ftr['end']):
                ftr['partial']['end'] = True
                ftr['end'] = re.sub(r'^[<>]', '', ftr['end'])
            
            ftr['start'] = int(ftr['start'])
            ftr['end'] = int(ftr['end'])
            
            for q in f.qualifiers:
                key = re.sub(r'^/|=$', '', q.key)
                val = re.sub(r'^"|"$', '', q.value)
                
                if key == 'gene':
                    ftr['gene_name'] = val
                elif key == 'codon_start':
                    ftr['phase'] = (int(val) - 1) % 3   # Make sure phase is 0, 1, or 2
                elif key == 'product':
                    ftr['product'] = val
                elif key == 'locus_tag':
                    ftr['gene_id'] = val
                elif key == 'protein_id':
                    ftr['protein_id'] = val
            
            if ftr['gene_id'] != '' and ftr['ftype'] == 'CDS':
                # If we have a CDS with a gene name, then we convert that to a 'gene'-type record.
                ftr['ftype'] = 'gene'
            
            if 'protein_id' in ftr and ftr['gene_id'] == '':
                ftr['gene_id'] = 'No Gene ID (protein: {:s})'.format(ftr['protein_id'])
            
            feat.append(ftr)
    
    resmsg = ''
    errmsg = ''                      # Error message. Defaults to 'Unknown error...'
    
    # Set up the dictionary of queries.
    q = {'add' : {},
         'get' : {}}
    
    q['add']['feature'] = """
    INSERT INTO feature (organism_id,
                         name,
                         uniquename,
                         residues,
                         seqlen,
                         type_id)
                 VALUES (%s,
                         %s,
                         %s,
                         %s,
                         %s,
                         %s);
    """
    
    q['add']['locn'] = """
    INSERT INTO featureloc (feature_id,
                            srcfeature_id,
                            fmin,
                            is_fmin_partial,
                            fmax,
                            is_fmax_partial,
                            strand,
                            phase)
                    VALUES (%s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s);
    """
    
    q['add']['prop'] = """
    INSERT INTO featureprop (feature_id,
                             type_id,
                             value,
                             rank)
                     VALUES (%s,
                             %s,
                             %s,
                             %s);
    """
    
    q['add']['org'] = """
    INSERT INTO organism (abbreviation,
                          genus,
                          species,
                          common_name)
                  VALUES (%s,
                          %s,
                          %s,
                          %s);
    """
    
    #   Query to get an organism ID.
    q['get']['org'] = """
    SELECT organism_id
      FROM organism
     WHERE genus = %s AND
           species = %s;
    """
    
    #   Query to get the CV Term ID for a Feature's Type.
    q['get']['ftype'] = """
    SELECT cvterm_id
      FROM cvterm
           JOIN cv ON cvterm.cv_id = cv.cv_id
     WHERE cvterm.name = %s AND cv.name = 'SO';
    """
    
    #   Query to get the CV Term ID for a Feature Property type.
    q['get']['ptype'] = """
    SELECT cvterm_id
      FROM cvterm
           JOIN cv ON cvterm.cv_id = cv.cv_id
     WHERE cvterm.name = %s AND cv.name = 'annotation_attributes.ontology';
    """
    
    organism = None
    
    # Check to see if we have an organism meeting the criteria in the database already.
    curs.execute(q['get']['org'], (org['genus'], org['species']))
    
    if curs.rowcount > 0:
        organism, = curs.fetchone()
    else:
        # If we didn't find any results, add the organism to the database.
        # Insert the organism data.
        curs.execute(q['add']['org'], (str(org['abbrev']),
                                       str(org['genus']),
                                       str(org['species']),
                                       str(org['common'])))
        
        count['organism'] += 1
        
        # Retrieve the ID for the newly inserted organism.
        organism = getlastid(curs)
    
    # Get the cvterm.cvterm_id value for the annotation's Type ID.
    curs.execute(q['get']['ftype'], ('assembly', ))
    
    atype, = curs.fetchone()
    
    # Add the annotation record to the feature table.
    curs.execute(q['add']['feature'], (organism, gb.locus, gb.accession[0], gb.sequence, len(gb.sequence), atype))
    count['feature'] += 1
    
    # Get the ID number of the annotation.
    annotn = getlastid(curs)
    
    for f in feat:
        seq = gb.sequence[f['start'] - 1:f['end']]
        
        if (f['strand'] < 0):
            # If this is a negative strand, get the reverse complement.
            seq = Seq.reverse_complement(seq)
        
        curs.execute(q['get']['ftype'], (str(f['ftype']), ))
        
        ftype, = curs.fetchone()
        
        # Convert gene name and gene ID to strings.
        for k in ['gene_name', 'gene_id']:
            f[k] = str(f[k])
        
        # IF the gene ID isn't blank, then we insert the record.
        if f['gene_id'] != '':
            curs.execute(q['add']['feature'], (organism,
                                               str(f['gene_name']),
                                               str(f['gene_id']),
                                               seq,
                                               len(seq),
                                               ftype))
            count['feature'] += 1
            
            # Get ID of the current feature.
            feature = getlastid(curs)
            
            # Store the location data.
            curs.execute(q['add']['locn'], (feature, annotn, f['start'],
                                            f['partial']['start'], f['end'],
                                            f['partial']['end'], f['strand'],
                                            f['phase']))
            count['featureloc'] += 1
            
            # Assign all relevant properties.
            
            #   Gene Product
            if 'product' in f:
                curs.execute(q['get']['ptype'], ('gene_product_name', ))
                ptype, = curs.fetchone()
                curs.execute(q['add']['prop'], (feature, ptype, f['product'], 0))
                count['featureprop'] += 1
    
    try:
        conn.commit()
    except Exception as e:
        # There was an error during the COMMIT action; store the error and rollback changes.
        resmsg = 'Unable to add Annotation {}.'.format(accn)
        errmsg = 'Unable to commit changes to database ({}).'.format(e)
        conn.rollback()
        
        # Reset the number of entries inserted.
        for k in count:
            count[k] = 0
    
    res = {'type' : 'new',
           'data' : [{'query' : accn,
                      'result' : resmsg,
                      'errmsg' : errmsg,
                      'added' : "\n".join(['{}: {}'.format(x, count[x]) for x in sorted({k:v for k, v in count.items() if v > 0})])}],
           'cols' : [{'name' : 'Query', 'id' : 'query'},
                     {'name' : 'Result', 'id' : 'result'},
                     {'name' : 'Errors', 'id' : 'errmsg'},
                     {'name' : 'Rows Added', 'id' : 'added'}]
           }
    
    # Return the results.
    return json.dumps(res)

# Main function
def main():
    print("Content-Type: application/json\n\n")
    form = cgi.FieldStorage()
    type = form.getvalue('srch_type')
    
    conn = mysql.connector.connect(user='rgrady7', password='projecting', host='localhost', database='rgrady7_chado')
    
    if type == "annotation":
        print(getannotation(form))
    elif type == "feature":
        print(getfeature(form))
    elif type == "add":
        print(add(form))
    else:
        print(json.dumps({'type' : 'error', 'message' : "Invalid search type '{:s}' received!".format(type)}))

# Run the main operation if this is called directly.
if __name__ == '__main__':
    main()